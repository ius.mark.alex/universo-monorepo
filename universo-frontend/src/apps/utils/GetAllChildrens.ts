export function getAllChildrens(obj) {
  // Проверяем, есть ли у объекта метод getChildren
  if (!obj.getChildren || typeof obj.getChildren !== 'function') {
    return;
  }

  let children = obj.getChildren(); // Получаем непосредственных потомков
  let allDescendants = [...children]; // Начальный массив для всех потомков

  // Итерация по каждому потомку и рекурсивный вызов для его потомков
  children.forEach((child) => {
    allDescendants = allDescendants.concat(getAllChildrens(child));
  });

  return allDescendants.filter((item) => item);
}
