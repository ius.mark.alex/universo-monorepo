import CreateMoreScrollablePanels from '../kanbans/CreateScrollablePanel';

export default (scene) => {
  const parent = scene.sys.game.canvas.parentElement;
  const parentWidth = parent.offsetWidth;
  const parentHeight = parent.offsetHeight;
  const numberOfPanels = scene.store.getTree.length;
  const panelHeight = (parentHeight - scene.mainMenuHeight) / numberOfPanels;

  const topPanelsArr = CreateMoreScrollablePanels(scene, scene.store.getTree, {
    h: panelHeight,
    w: parentWidth,
  });
  for (let i = 0; i < topPanelsArr.length; i++) {
    topPanelsArr[i]
      .setPosition(
        parentWidth / 2,
        panelHeight / 2 + i * panelHeight + scene.mainMenuHeight,
      )
      .layout();
  }
};
