## В этой папке содержатся примеры диаграмм

#### Для редактирования и использования в vscode можно использовать следующие плагины

Name: **UMLet**

Description: Free UML Tool for Fast UML Diagrams

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=TheUMLetTeam.umlet

Name: **Markdown Preview Mermaid Support**

Description: Adds Mermaid diagram and flowchart support to VS Code's builtin markdown preview

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid

Name: **Mermaid Markdown Syntax Highlighting**

Description: Markdown syntax support for the Mermaid charting language

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=bpruitt-goddard.mermaid-markdown-syntax-highlighting

Name: **Mermaid Editor**

Description: Live editor for mermaid.js in Visual Studio Code

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=tomoyukim.vscode-mermaid-editor

Name: **classdiagram-ts**

Description: Generates class diagram for typescript projects

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=AlexShen.classdiagram-ts

Name: **Draw.io Integration**

Description: This unofficial extension integrates Draw.io into VS Code.

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio
