# import graphene
# import graphql_jwt


# class Mutation(graphene.ObjectType):
#     token_auth = graphql_jwt.ObtainJSONWebToken.Field()
#     verify_token = graphql_jwt.Verify.Field()
#     refresh_token = graphql_jwt.Refresh.Field()


# schema = graphene.Schema(mutation=Mutation)
# _____________

import graphene
import graphql_jwt
from wagtail.api.v2.views import PagesAPIViewSet
from wagtail.api.v2.router import WagtailAPIRouter
from wagtail.api.v2.utils import BadRequestError
from wagtail.models import Page
from wagtail.users.models import UserProfile
from graphene_django import DjangoObjectType
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model

class UserType(DjangoObjectType):
    class Meta:
        model = AbstractUser
        fields = "__all__"
        id = graphene.ID()
        username = graphene.String()
        email = graphene.String()
        
        # slug = graphene.String()

class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user)

class RefreshMutation(graphql_jwt.Refresh):
    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user)

class RevokeMutation(graphql_jwt.Revoke):
    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user)

router = WagtailAPIRouter('wagtailapi')
router.register_endpoint('pages', PagesAPIViewSet)

class Query(graphene.ObjectType):
    # pages = graphene.List(PageType)
    app_users=graphene.Field(UserType)

    # def resolve_pages(self, info):
    #     return Page.objects.all()

    # def resolve_app_users(self, info):
    #     return get_user_model().objects.all()

    def resolve_app_users(self, info):
        return UserType.objects.all()

class CreateAppUser(graphene.Mutation):
    app_user = graphene.Field(UserType)

    class Arguments:
        email = graphene.String()
        username = graphene.String()
        # nickname = graphene.String()
        password = graphene.String()

    def mutate(self, info, email, username, password):
        app_user = UserType
        new_user = app_user(email=email,username=username)
        new_user.set_password(password)
        new_user.save()
        return CreateAppUser(app_user=new_user)

class Mutation(graphene.ObjectType):
    create_app_user = CreateAppUser.Field()
    token_auth = ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = RefreshMutation.Field()
    revoke_token = RevokeMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)


# ______________
# import graphene
# import graphql_jwt
# # from wagtail.core.models import Page

# class PageType(graphene.ObjectType):
#     id = graphene.ID()
#     title = graphene.String()
#     slug = graphene.String()

# # class Query(graphene.ObjectType):
# #     pages = graphene.List(PageType)

# #     def resolve_pages(self, info):
# #         return Page.objects.all()

# class Query(graphene.ObjectType):
#     viewer = graphene.Field(UserType)

#     def resolve_viewer(self, info, **kwargs):
#         user = info.context.user
#         if not user.is_authenticated:
#             raise Exception("Authentication credentials were not provided")
#         return user

# class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
#     user = graphene.Field(UserType)

#     @classmethod
#     def resolve(cls, root, info, **kwargs):
#         return cls(user=info.context.user)

# class RefreshJSONWebToken(graphql_jwt.RefreshMutation):
#     user = graphene.Field(UserType)

#     @classmethod
#     def resolve(cls, root, info, **kwargs):
#         return cls(user=info.context.user)

# class Mutation(graphene.ObjectType):
#     token_auth = ObtainJSONWebToken.Field()
#     refresh_token = RefreshJSONWebToken.Field()

# # class Mutation(graphene.ObjectType):
# #     token_auth = graphql_jwt.ObtainJSONWebToken.Field()
# #     verify_token = graphql_jwt.Verify.Field()
# #     refresh_token = graphql_jwt.Refresh.Field()

# # schema = graphene.Schema(query=Query, mutation=Mutation)
# schema = graphene.Schema(mutation=Mutation)